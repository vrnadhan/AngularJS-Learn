tsApp.directive("accordion", function () {
  return {
    template: "<div ng-transclude></div>",
    restrict: "E",
    transclude: true,
    controller: function ($scope, $element, $attrs, $transclude) {
      var accordionItems = [];

      var addAccordionItem = function (accordionScope) {
        accordionItems.push(accordionScope);
      };

      var closeAll = function () {
        angular.forEach(accordionItems, function (accordionScope) {
          accordionScope.active = false;
        });
      };

      return {
        addAccordionItem: addAccordionItem,
        closeAll: closeAll
      };
    }
  };
});

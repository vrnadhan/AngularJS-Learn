//templateUrl: "alert.html" -- it will have the below HTML
/*template:   "<div class='alert'>" +
    "<span class='alert-topic'>" +
      "Something went wrong!" +
    "</span>" +
    "<span class='alert-description'>" +
      "You must inform the plate and the color of the car!" +
    "</span>" +
  "</div>",*/
//<div alert></div> - directive is specified as an attribute of the element.
// to make a directive as a reusable component, doesnt make much sense to restrict it as
// an attribute, but as an element.
// default - restrict : A
// Attribute (default) - A - <div alert></div>
// Element name - E - <alert></alert>
// Class - C - <div class="alert"></div>
// Comment - M - <!-- directive:alert -->
//If the directive is applied without the restrictions configuration, it will be ignored by the framework.

// scope
// @ - This prefix passes the data as a string.
// = - This prefix creates a bidirectional relationship between a controller's scope property and a local scope directive property. {{}}
// & - This prefix binds the parameter with an expression in the context of the parent scope. It is useful if you would like
// to provide some outside functions to the directive.
tsApp.directive("alert", function () {
  return {
    restrict: 'E',
    templateUrl: "/templates/alert.html",
    scope: {
      topic: '=alerttopic',
      description: '=',
      close : '&'
    },
    replace: true
  };
});

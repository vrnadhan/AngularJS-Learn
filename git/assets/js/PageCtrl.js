"use strict";
/**
 * Controls all other Pages
 */
tsApp.controller('PageCtrl', ['$scope', function ($scope) {
  console.log("Page Controller reporting for duty.");

  // Activates the Carousel
  $('.carousel').carousel({
    interval: 5000
  });

  // Activates Tooltips for Social Links
  $('.tooltip-social').tooltip({
    selector: "a[data-toggle=tooltip]"
  });


  // learn - ANGULAR
  //bind a string of raw HTML
  //In order to use this directive, we will need the angular-sanitize.js dependency
  //$scope.appTitle = "<b>[Packt] Parking</b>"; -- unsafe error on console.
  $scope.appTitle = "[Packt] Parking";
  $scope.cars = [];
  $scope.parkcar = "Park Car?"
  $scope.car = {};
   $scope.colors = ["White", "Black", "Blue", "Red", "Silver"];
   $scope.alertTopic = "Something went wrong!";
  $scope.alertMessage = "You must inform the plate and the color of the car!";
  $scope.showParkingError = false;
  $scope.park = function (car) {
    if(car && car.plate && car.plate != "") {
      car.entrance = new Date();
      $scope.cars.push(car);
      delete $scope.car;
    } else {
      // nothing.
      $scope.showParkingError = true;
    }
  };

  // for ng-repeat we can use $index, $first, $last, $middle, $even, $odd
  //Other directives that have the same behavior but are triggered by other events are ngBlur, ngChange, ngCopy, ngCut, ngDblClick,
  //ngFocus, ngKeyPress, ngKeyDown, ngKeyUp, ngMousedown, ngMouseenter, ngMouseleave, ngMousemove, ngMouseover, ngMouseup, and ngPaste.
  $scope.deletecar = function (index) {
    $scope.cars.splice(index,1)
  };
  $scope.closeAlertPE = function () {
    $scope.showParkingError = false;
  };
}]);
